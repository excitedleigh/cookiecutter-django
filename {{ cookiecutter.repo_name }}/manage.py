#!/usr/bin/env python
import os
import sys

from django import setup


def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{ cookiecutter.pkg_name }}.settings")

    from django.core.management import execute_from_command_line
    setup()
    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    main()
