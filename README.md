# cookiecutter-django

A Cookiecutter template for Django projects. Features:

- **Yak-free development**
    - **Python 3 only**: With Python 2's EoL in 2020, and most libraries supporting Python 3, there's no good reason not to.
    - **Default templates using Bootstrap**: Bootstrap is often a good-enough option, especially for a lot of quick-and-dirty or internal-only apps, or as a starting point for building out functionality.
    - **Stub accounts app using `django.contrib.auth.views`**: Gives you a 100% working login from the start.
    - **GitLab CI pre-configured**: Tests are no use if you never run them. Also runs style checks and builds wheels.
    - **Sensible code styles, enforced**: Python code style is PEP 8; JavaScript and CSS is Prettier. Editor configuration is kept consisted with [editorconfig](http://editorconfig.org).
    - **Code style and static analysis checks with pre-commit**: Keeps your code readable and consistent, and catches some kinds of potential errors early.
- **Simplified deployment**
    - *optional* **Buildpack-friendly layout**: Deploy straight to Heroku, Cloud Foundry, and more.
    - **Single-package layout with wheel support**: Deploy your Django project onto a server with a single `pip install`.
    - **Configuration with python-decouple**: Some things in settings.py should be configured by devs (like `INSTALLED_APPS`), some at deployment time (like `ALLOWED_HOSTS`). This separates them.
    - **Built-in support for Waitress**: Comes with a management command `your-project serve`, which runs a production-ready server that's safe to use with slow clients.
    - [*coming soon*](https://gitlab.com/abre/cookiecutter-django/issues/6) **Zero-configuration static and media file serving**: Static files and media that Just Work out of the box in both development and production.
- **Improved security**
    - **Content-Security-Policy pre-configured**: CSP is your last line of defence against XSS and similar attacks, and its a *lot* easier to start with CSP than to add it later.
    - *optional* **Require login by default with django-stronghold**: Makes it a lot harder to accidentally leak data to the world, if you're building the sort of app that is mostly used only when logged in.
    - **Argon2 password storage enabled**: Django recommends using argon2 for password storage, but doesn't default to it, as it requires a C extension. In practice, C extensions aren't usually an issue, so we enable it.
    - *optional* **LDAP pre-configured for Active Directory**: For internal apps, don't force your users to remember *yet another* password; let them log in with their AD password instead.

## Usage

```shell
pip install pre-commit cookiecutter
cookiecutter https://gitlab.com/abre/cookiecutter-django.git
cd myproject
pip install -e .
setup-freeze
createdb myproject
myproject migrate
myproject runserver
```

## Developing the Cookiecutter

This cookiecutter comes with a series of unit tests, which run the cookiecutter with a variety of different parameters, to verify that it will actually build, and to check that it doesn't produce files that pre-commit will complain about out of the box.

To run these tests:

```shell
pyenv virtualenv 3.6.5 cookiecutter-django
pyenv local 3.6.5/envs/cookiecutter-django
# ...or however you like to manage your virtualenvs
pip install -r requirements-test.txt
pytest
```

## License

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <span resource="[_:publisher]" rel="dct:publisher">
    <span property="dct:title">Commercial Motor Vehicles Pty Ltd</span></span>
  have waived all copyright and related or neighboring rights to
  <span property="dct:title">cookiecutter-django</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="AU" about="[_:publisher]">
  Australia</span>.
</p>
