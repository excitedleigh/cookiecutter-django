#!/bin/bash

set -e

HEADDIR=$(mktemp -d)
HEADBUILT=$(mktemp -d)
CURRBUILT=$(mktemp -d)
git checkout-index -af --prefix=$HEADDIR/
cookiecutter --no-input -o $CURRBUILT . initial_commit=n
cookiecutter --no-input -o $HEADBUILT $HEADDIR initial_commit=n
ksdiff $HEADBUILT $CURRBUILT