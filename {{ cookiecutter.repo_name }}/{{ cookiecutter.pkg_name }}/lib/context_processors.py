from django.conf import settings


def rollbar(request):
    if getattr(settings, 'ROLLBAR', False):
        return {
            'rollbar': {
                'token': settings.ROLLBAR_CLIENT_TOKEN,
                'environment': settings.ROLLBAR['environment'],
            }
        }
    return {}
