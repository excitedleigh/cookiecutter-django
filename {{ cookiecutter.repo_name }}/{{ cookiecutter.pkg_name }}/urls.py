from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView
{% if cookiecutter.cms == 'wagtail' %}
from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtaildocs import urls as wagtaildocs_urls
from wagtail.wagtailcore import urls as wagtail_urls
{% endif %}
urlpatterns = [
    url(r'^
        {%- if cookiecutter.cms == 'wagtail' %}django-{% endif -%}
    admin/', include(admin.site.urls)),
    {%- if cookiecutter.cms == 'none' %}
    url(r'^accounts/', include('{{ cookiecutter.pkg_name }}.apps.accounts.urls', namespace='accounts')),
    {%- endif %}

    # Redirect favicon.ico to staticfiles, in case we didn't serve the <link>
    # tag in the view for whatever reason
    url(
        r'^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('favicon.ico'),
            permanent=False),
        name="favicon"
    ),
{% if cookiecutter.cms == 'django_cms' %}
    # Catch all other URLs with Django CMS
    url(r'', include('cms.urls')),
{% endif %}{% if cookiecutter.cms == 'wagtail' %}
    url(r'^cms/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),
    # Catch all other URLs with Wagtail
    url(r'', include(wagtail_urls)),
{% endif %}]

if settings.DEBUG and settings.MEDIA_URL and settings.MEDIA_ROOT:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
