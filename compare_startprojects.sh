#!/usr/bin/env bash

# Compares the output of `startproject` for different versions of Django.
# Useful for updating this template as new versions are released.

set -e
export DJANGO_SETTINGS_MODULE=

TMPDIR=`mktemp -d`
cd $TMPDIR

virtualenv v1 >&2
v1/bin/pip install Django==$1 >&2
virtualenv v2 >&2
v2/bin/pip install Django==$2 >&2
mkdir p1
cd p1
../v1/bin/django-admin startproject projectname >&2
mkdir ../p2
cd ../p2
../v2/bin/django-admin startproject projectname >&2
cd ..
diff p1 p2

cd
rm -rf $TMPDIR
