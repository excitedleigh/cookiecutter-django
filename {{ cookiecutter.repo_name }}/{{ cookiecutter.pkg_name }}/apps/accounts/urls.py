from django.conf import settings
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import permission_required
{%- if cookiecutter.use_stronghold == "y" %}
from stronghold.decorators import public
{%- endif %}

add_user = permission_required('auth.add_user')
change_user = permission_required('auth.change_user')
add_group = permission_required('auth.add_group')
change_group = permission_required('auth.change_group')

{% if cookiecutter.use_stronghold != "y" %}
def public(x):
    return x
{%- endif %}

urlpatterns = [
    url(r'^login/$', public(auth_views.LoginView.as_view(
        template_name='accounts/login.html',
        extra_context={'title': 'Log In'},
    )), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name="logout"),
    url(r'^change-password/$', auth_views.PasswordChangeView.as_view(
        template_name='base_form.html',
        extra_context={'title': 'Change Password'},
        success_url=reverse_lazy('accounts:password_change_done'),
    ), name="password_change"),
    url(r'^change-password/done/$', auth_views.PasswordChangeDoneView.as_view(
        template_name='base.html',
        extra_context={
            'title': 'Password Change Done',
            'body': 'Your password has been changed.',
        },
    ), name="password_change_done"),
    url(r'^reset-password/$', public(auth_views.PasswordResetView.as_view(
        template_name='accounts/base_form.html',
        email_template_name='accounts/password_reset_email.txt',
        from_email=settings.EMAIL_FROM_ADDRESS,
        extra_context={'title': 'Reset Password'},
        success_url=reverse_lazy('accounts:password_reset_done'),
    )), name="password_reset"),
    url(r'^reset-password/check-your-email/$',
        public(auth_views.PasswordResetDoneView.as_view(
            template_name='accounts/base.html',
            extra_context={
                'title': 'Reset Password',
                'body': 'Please check your email.',
            },
        )), name="password_reset_done"),
    url(r'^reset-password/confirm/(?P<uidb64>[^/]+)/(?P<token>[^/]+)/$',
        public(auth_views.PasswordResetConfirmView.as_view(
            template_name='accounts/base_form.html',
            extra_context={'title': 'Reset Password'},
            success_url=reverse_lazy('accounts:password_reset_complete'),
        )), name="password_reset_confirm"),
    url(r'^reset-password/complete/$',
        public(auth_views.PasswordResetCompleteView.as_view(
            template_name='accounts/reset_complete.html',
        )), name="password_reset_complete"),
]
