const path = require('path')
const webpack = require('webpack')
const BundleTracker = require('webpack-bundle-tracker')
const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin")

const extractCss = new ExtractTextPlugin({
    filename: "[contenthash].css",
})

module.exports = {
    entry:  './frontend',
    output: {
        path:     path.resolve(__dirname, '{{ cookiecutter.pkg_name }}/static/build'),
        filename: '[chunkhash].js',
        publicPath: '/static/build/',{# TODO: dynamically set this #}
    },
    plugins: [
        new CommonsChunkPlugin({children: true, async: true}),
        new BundleTracker({
            filename: '{{ cookiecutter.pkg_name }}/webpack-metadata.json',
        }),
        extractCss,
    ],
    resolve: {
      extensions: ['.src.js', '.js', '*'],
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: extractCss.extract({
                    use: [
                        {loader: "css-loader"},
                        {loader: "sass-loader"},
                    ],
                }),
            },
            {
                test: /\.css$/,
                use: extractCss.extract({
                    use: [
                        {loader: "css-loader"},
                    ],
                }),
            },
        ],
    },
}
