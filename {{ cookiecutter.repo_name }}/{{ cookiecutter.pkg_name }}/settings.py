import os
from urllib.parse import urlparse

import pkg_resources
{% if cookiecutter.ldap == "y" %}
import ldap
{%- endif %}
from decouple import Csv, config
from dj_database_url import parse as db_url
from dj_media_url import media_url
{%- if cookiecutter.ldap == "y" %}
from django_auth_ldap.config import ActiveDirectoryGroupType, LDAPSearch
{%- endif %}

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
BASE_DIR = os.path.dirname(PROJECT_DIR)

SECRET_KEY = config('SECRET_KEY')
DEBUG = config('DEBUG', cast=bool, default=False)
ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=Csv())

{%- if cookiecutter.cms == 'wagtail' %}
WAGTAIL_SITE_NAME = '{{ cookiecutter.dist_name }}'
{%- endif %}


INSTALLED_APPS = (
    {%- if cookiecutter.cms == 'django_cms' %}
    # Django CMS admin overrides (must be above django.contrib.admin)
    'djangocms_admin_style',
{% endif %}
    # Django builtin
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    {%- if not cookiecutter.api_only == "y" %}
    'django.contrib.messages',
    'django.contrib.staticfiles',
    {%- endif %}

    {%- if cookiecutter.cms == 'django_cms' %}
    # Django CMS
    'django.contrib.sites',
    'cms',
    'treebeard',
    'menus',
    'sekizai',
    'reversion',

    # Django CMS core-ish plugins
    # These can be removed if required
    'djangocms_googlemap',
    'djangocms_inherit',
    'djangocms_link',
    'easy_thumbnails',
    'filer',
    'mptt',
    'djangocms_text_ckeditor',
    'cmsplugin_filer_file',
    'cmsplugin_filer_folder',
    'cmsplugin_filer_link',
    'cmsplugin_filer_image',
    'cmsplugin_filer_teaser',
    'cmsplugin_filer_video',
    {%- endif %}
    {%- if cookiecutter.cms == 'wagtail' %}
    # Wagtail
    'wagtail.wagtailforms',
    'wagtail.wagtailredirects',
    'wagtail.wagtailembeds',
    'wagtail.wagtailsites',
    'wagtail.wagtailusers',
    'wagtail.wagtailsnippets',
    'wagtail.wagtaildocs',
    'wagtail.wagtailimages',
    'wagtail.wagtailsearch',
    'wagtail.wagtailadmin',
    'wagtail.wagtailcore',
    'modelcluster',
    'taggit',
    {%- endif %}

    # Third party
    {%- if not cookiecutter.api_only == "y" %}
    'form_utils',
    {%- endif %}
    'raven.contrib.django.raven_compat',
    {%- if cookiecutter.use_stronghold == "y" %}
    'stronghold',
    {%- endif %}
    'spurl',
    {%- if cookiecutter.api_only != "y" %}
    'webpack_loader',
    {%- endif %}
    'widget_tweaks',

    # First party
    '{{ cookiecutter.pkg_name }}.lib',
    {%- if cookiecutter.cms == 'none' %}
    '{{ cookiecutter.pkg_name }}.apps.accounts',
    {%- endif %}
)


{%- if cookiecutter.cms == 'django_cms' %}
SITE_ID = 1
{% endif %}

MIDDLEWARE = (
    # Middleware that has to come first
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    {%- if cookiecutter.cms == 'django_cms' %}
    'cms.middleware.utils.ApphookReloadMiddleware',
    {%- endif %}

    # Django builtin
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    {%- if cookiecutter.cms == 'django_cms' %}

    # Django CMS
    'django.middleware.locale.LocaleMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    {%- endif %}
    {%- if cookiecutter.cms == 'wagtail' %}
    'wagtail.wagtailcore.middleware.SiteMiddleware',
    'wagtail.wagtailredirects.middleware.RedirectMiddleware',
    {%- endif %}

    # Third party
    {%- if not cookiecutter.api_only == "y" %}
    'csp.middleware.CSPMiddleware',
    {%- endif %}
    {%- if cookiecutter.use_stronghold == "y" %}
    'stronghold.middleware.LoginRequiredMiddleware',
    {%- endif %}

    # First party
    '{{ cookiecutter.pkg_name }}.lib.middleware.XUACompatibleMiddleware',
)

ROOT_URLCONF = '{{ cookiecutter.pkg_name }}.urls'
{% if not cookiecutter.api_only == "y" %}
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(PROJECT_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Django builtin
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                {%- if cookiecutter.cms == 'django_cms' %}
                'django.template.context_processors.i18n',

                # Django CMS
                'sekizai.context_processors.sekizai',
                'cms.context_processors.cms_settings',
                {%- endif %}
            ],
        },
    },
]
{% endif %}
WSGI_APPLICATION = '{{ cookiecutter.pkg_name }}.wsgi.application'


# Database
DATABASES = {
    'default': config('DATABASE_URL', cast=db_url),
}


# Cache
_redis = config('REDIS_URL', default=None)
if _redis:
    CACHES = {
        "default": {
            "BACKEND": "django_redis.cache.RedisCache",
            "LOCATION": _redis,
            "OPTIONS": {
                "CLIENT_CLASS": "django_redis.client.DefaultClient",
            }
        }
    }


# Internationalization
LANGUAGE_CODE = 'en-au'
TIME_ZONE = 'Australia/Adelaide'
USE_I18N = True
USE_L10N = True
USE_TZ = True
{% if cookiecutter.cms == 'django_cms' %}
# Django CMS
CMS_TEMPLATES = (
    ('cmstemplates/basic_page.html', 'Basic Page'),
)
LANGUAGES = [
    ('en-au', 'English'),
]
{% endif %}
{% if not cookiecutter.api_only == "y" %}
# Static files (CSS, JavaScript, Images)
STATIC_ROOT = config('STATIC_ROOT', default=os.path.join(
    PROJECT_DIR, 'collected-static'))
STATIC_URL = '/static/'
STATICFILES_DIRS = [os.path.join(PROJECT_DIR, 'static')]
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'{% if cookiecutter.api_only != "y" %}
WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'build/',
        'STATS_FILE': os.path.join(PROJECT_DIR, 'webpack-metadata.json'),
    }
}
{% endif %}{% if cookiecutter.cms == 'django_cms' %}
THUMBNAIL_HIGH_RESOLUTION = True
THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)
{% endif %}

# User-uploaded media
globals().update(config('MEDIA_BACKEND_URL', cast=media_url,
                        default='file:///./media/'))
{% endif %}

# Users
LOGIN_URL = 'login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL =
{%- if cookiecutter.use_stronghold == 'y' %} 'accounts:login'
{%- else %} '/'
{%- endif %}
PASSWORD_HASHERS = (
    # Add new password hashers to the top of this list to make Django
    # automatically upgrade users as they log in
    'django.contrib.auth.hashers.Argon2PasswordHasher',
)
{% if cookiecutter.ldap == "y" %}AUTHENTICATION_BACKENDS = (
    'django_auth_ldap.backend.LDAPBackend',
    'django.contrib.auth.backends.ModelBackend',
)
AUTH_LDAP_CONNECTION_OPTIONS = {
    ldap.OPT_REFERRALS: 0
}
AUTH_LDAP_SERVER_URI = config('LDAP_SERVER')
AUTH_LDAP_BIND_DN = config('LDAP_BIND_DN')
AUTH_LDAP_BIND_PASSWORD = config('LDAP_BIND_PASSWORD')
AUTH_LDAP_USER_SEARCH = LDAPSearch(config(
    'LDAP_SEARCH_BASE'), ldap.SCOPE_SUBTREE, "(sAMAccountName=%(user)s)")
AUTH_LDAP_ALWAYS_UPDATE_USER = True
AUTH_LDAP_USER_ATTR_MAP = {
    "first_name": "givenName",
    "last_name": "sn",
    "email": "mail"
}
AUTH_LDAP_MIRROR_GROUPS = True
AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
    config('LDAP_SEARCH_BASE'), ldap.SCOPE_SUBTREE, "(objectClass=group)")
AUTH_LDAP_GROUP_TYPE = ActiveDirectoryGroupType()
AUTH_LDAP_USER_FLAGS_BY_GROUP = {
    "is_staff": config('LDAP_STAFF_FLAG'),
    "is_superuser": config('LDAP_SUPER_FLAG'),
}
AUTH_LDAP_FIND_GROUP_PERMS = True
AUTH_LDAP_CACHE_GROUPS = False
AUTH_LDAP_GROUP_CACHE_TIMEOUT = 3600
{% endif %}

# Email
EMAIL_FROM_ADDRESS = config('EMAIL_FROM_ADDRESS',
                            default='{{ cookiecutter.dist_name }}@example.com')

if config('EMAIL_TO_CONSOLE', cast=bool, default=DEBUG):
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
{% if cookiecutter.heroku == "y" %}elif config('MAILGUN_API_KEY', default=''):
    EMAIL_HOST = config('MAILGUN_SMTP_SERVER')
    EMAIL_PORT = config('MAILGUN_SMTP_PORT')
    EMAIL_HOST_USER = config('MAILGUN_SMTP_LOGIN')
    EMAIL_HOST_PASSWORD = config('MAILGUN_SMTP_PASSWORD')
    EMAIL_USE_TLS = True
{% endif %}else:
    EMAIL_HOST = config('EMAIL_HOST')
    EMAIL_HOST_USER = config('EMAIL_HOST_USER', default='')
    EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD', default='')
    _email_tls = config('EMAIL_TLS', default='explicit')
    if _email_tls == 'explicit':
        EMAIL_USE_TLS = True
        _default_port = 587
    elif _email_tls == 'implicit':
        EMAIL_USE_SSL = True
        _default_port = 465
    elif _email_tls == 'none':
        _default_port = 25
    else:
        raise ValueError('EMAIL_TLS must be one of explicit, implicit, none')
    EMAIL_PORT = config('EMAIL_PORT', cast=int, default=_default_port)

# Security
USE_X_FORWARDED_HOST = config('USE_X_FORWARDED_HOST', default=False)
SECURE_PROXY_SSL_HEADER = config('SECURE_PROXY_SSL_HEADER',
                                 cast=lambda x: x.split(':', 1) if x else None,
                                 default=None)
SECURE_HSTS_SECONDS = 31556952  # seconds in an average year
{% if not cookiecutter.api_only == "y" %}
CSP_DEFAULT_SRC = ("'self'",){% if cookiecutter.cms == 'wagtail' %}
CSP_IMG_SRC = CSP_DEFAULT_SRC + ("www.gravatar.com",){% endif %}{% if cookiecutter.cms == 'django_cms' %}
CSP_SCRIPT_SRC = CSP_DEFAULT_SRC + ("'unsafe-inline'", "'unsafe-eval'")
CSP_IMG_SRC = CSP_DEFAULT_SRC + ("data:",){% endif %}
CSP_STYLE_SRC = CSP_DEFAULT_SRC + ("https://cdn.jsdelivr.net",)
CSP_FONT_SRC = CSP_DEFAULT_SRC + ("https://cdn.jsdelivr.net",)

# Note: base-uri, frame-ancestors and form-action do not fall back to default
# if they are omitted.
CSP_BASE_URI = ("'self'",)
CSP_FRAME_ANCESTORS = {% if cookiecutter.cms == 'django_cms' -%}
("'self'",)
{%- else -%}
("'none'",)
{%- endif %}
CSP_FORM_ACTION = CSP_DEFAULT_SRC
{% endif %}

# Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'WARNING',
        'handlers': ['console'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'propagate': False,
        },
        'django.request': {
            # Suppress 404s, which are logged at WARNING level
            'level': 'ERROR',
            'propagate': False,
        },
    },
}

# Error handling
_sentry_dsn = config('SENTRY_DSN', default=config('RAVEN_DSN', default=None))
if _sentry_dsn is not None:
    RAVEN_CONFIG = {
        'dsn': _sentry_dsn,
        'release': pkg_resources.get_distribution(
            "{{ cookiecutter.dist_name }}").version,
    }

    LOGGING['handlers']['sentry'] = {
        'level': 'INFO',
        'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
    }
    LOGGING['root']['handlers'].append('sentry')
    LOGGING['loggers']['raven'] = {
        'level': 'DEBUG',
        'handlers': ['console'],
        'propagate': False,
    }
    LOGGING['loggers']['sentry.errors'] = {
        'level': 'DEBUG',
        'handlers': ['console'],
        'propagate': False,
    }

    {% if not cookiecutter.api_only == "y" %}# Ensure the Raven JS client can access the server
    _raven_url = urlparse(_sentry_dsn)
    _raven_hostname = _raven_url.netloc.split('@', 1)[1]
    if 'CSP_CONNECT_SRC' not in globals():
        CSP_CONNECT_SRC = CSP_DEFAULT_SRC
    CSP_CONNECT_SRC = CSP_CONNECT_SRC + (
        "{}://{}".format(_raven_url.scheme, _raven_hostname),)
    if 'CSP_SCRIPT_SRC' not in globals():
        CSP_SCRIPT_SRC = CSP_DEFAULT_SRC
    CSP_SCRIPT_SRC += ("https://cdn.ravenjs.com",)
{% endif %}

_rollbar_token = config('ROLLBAR_SERVER_TOKEN', default=None)
if _rollbar_token:
    MIDDLEWARE += (
        'rollbar.contrib.django.middleware.RollbarNotifierMiddleware',
    )
    _rollbar_env = config('ROLLBAR_ENVIRONMENT',
                          default='development' if DEBUG else 'production')
    ROLLBAR = {
        'access_token': _rollbar_token,
        'environment': _rollbar_env,
        'branch': 'master',
        'root': PROJECT_DIR,
    }
    ROLLBAR_CLIENT_TOKEN = config('ROLLBAR_CLIENT_TOKEN', default=None)
    LOGGING['root']['handlers'].append('rollbar')
    LOGGING['handlers']['rollbar'] = {
        'access_token': _rollbar_token,
        'environment': _rollbar_env,
        'class': 'rollbar.logger.RollbarHandler'
    }

{% if not cookiecutter.api_only == "y" %}    TEMPLATES[0]['OPTIONS']['context_processors'].append(
        '{{ cookiecutter.pkg_name }}.lib.context_processors.rollbar',
    )
    if 'CSP_CONNECT_SRC' not in globals():
        CSP_CONNECT_SRC = CSP_DEFAULT_SRC
    CSP_CONNECT_SRC += ("https://api.rollbar.com",)
{% endif %}
