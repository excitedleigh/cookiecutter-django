import os
import shutil
import subprocess

os.rename('_gitignore', '.gitignore')
shutil.copyfile('example-settings.ini', 'settings.ini')

if '{{ cookiecutter.heroku }}' != 'y':
    os.unlink('Procfile')
    os.unlink('runtime.txt')

if '{{ cookiecutter.cms }}' != 'none':
    # The accounts app is not necessary when using a CMS
    shutil.rmtree(os.path.join('{{ cookiecutter.pkg_name }}', 'apps', 'accounts'))

if '{{ cookiecutter.api_only }}' == 'y':
    shutil.rmtree(os.path.join('{{ cookiecutter.pkg_name }}', 'apps', 'accounts'))
    shutil.rmtree(os.path.join('{{ cookiecutter.pkg_name }}', 'static'))
    shutil.rmtree(os.path.join('{{ cookiecutter.pkg_name }}', 'templates'))
    shutil.rmtree('frontend')
    os.unlink('MANIFEST.in')
    os.unlink('webpack.config.js')
    os.unlink('package.json')


if '{{ cookiecutter.cms }}' != 'django_cms':
    shutil.rmtree(os.path.join('{{ cookiecutter.pkg_name }}', 'templates', 'cms_pages'))


if '{{ cookiecutter.initial_commit }}' == 'y':
    print("Initialising Git repository...")
    subprocess.check_call(('git', 'init', '.'))
    print("Setting up Pipenv (will also generate lock file)...")
    subprocess.check_call(('pipenv', 'install', '-d'))
    print("Autoupdating pre-commit...")
    subprocess.check_call(('pre-commit', 'autoupdate'))
    print("Running npm install (to generate lockfile)...")
    subprocess.check_call(('npm', 'install'))
    print("Creating initial commit...")
    subprocess.check_call(('git', 'add', '.'))
    subprocess.check_call(('git', 'commit', '--no-verify', '-m', 'Initial commit'))
    print("Setting up pre-commit...")
    subprocess.check_call(('pre-commit', 'install'))
