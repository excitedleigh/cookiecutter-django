"""Some basic tests to ensure that your auth views work."""

import re

from django.contrib.auth.models import User
from django.core import mail
from django.core.urlresolvers import reverse
from django.test import Client, TestCase


class TestLoginProcess(TestCase):

    def test_login(self):
        user = User(username='user')
        user.set_password('hunter2')
        user.save()

        c = Client()
        login_get = c.get(reverse('accounts:login'))
        self.assertEqual(login_get.status_code, 200)
        login_post = c.post(reverse('accounts:login'), {
            'username': user.username,
            'password': 'hunter2',
        })
        self.assertEqual(login_post.status_code, 302)
        self.assertEqual(int(c.session['_auth_user_id']), user.pk)

    def test_reset_password(self):
        user = User(username='user', email='user@example.com')
        user.set_password('hunter1')
        user.save()

        c = Client()
        login_get = c.get(reverse('accounts:password_reset'))
        self.assertEqual(login_get.status_code, 200)
        login_post = c.post(reverse('accounts:password_reset'), {
            'email': user.email,
        })
        self.assertEqual(login_post.status_code, 302)
        self.assertTrue(login_post['Location'].endswith(
            reverse('accounts:password_reset_done')))
        self.assertEqual(len(mail.outbox), 1)

        done_get = c.get(reverse('accounts:password_reset_done'))
        self.assertEqual(done_get.status_code, 200)

        confirm_url = re.search(r'https?://\S+', mail.outbox[0].body).group()
        confirm_get = c.get(confirm_url)
        self.assertEqual(confirm_get.status_code, 302)
        confirm_redirect_url = confirm_get['Location']
        print(confirm_redirect_url)
        confirm_redirect_get = c.get(confirm_redirect_url)
        self.assertEqual(confirm_redirect_get.status_code, 200)
        confirm_post = c.post(confirm_redirect_url, {
            'new_password1': 'hunter2',
            'new_password2': 'hunter2',
        })
        self.assertEqual(confirm_post.status_code, 302)
        self.assertTrue(confirm_post['Location'].endswith(
            reverse('accounts:password_reset_complete')))

        complete_get = c.get(reverse('accounts:password_reset_complete'))
        self.assertEqual(complete_get.status_code, 200)

        user = User.objects.get(pk=user.pk)

        self.assertFalse(user.check_password('hunter1'))
        self.assertTrue(user.check_password('hunter2'))
