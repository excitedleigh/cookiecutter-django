from django.core.management.base import BaseCommand
from waitress import serve

from ....wsgi import application


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('host', nargs='?', default='127.0.0.1')
        parser.add_argument('port', nargs='?', type=int, default=8000)
        parser.add_argument('--unix', dest='unix', default=None)

    def handle(self, *args, **options):
        if options['unix'] is not None:
            serve(application, unix_socket=options['unix'])
        else:
            serve(application, host=options['host'], port=options['port'])
