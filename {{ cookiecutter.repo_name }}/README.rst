{{ cookiecutter.dist_name }}
=============================

Setting Up for Development
--------------------------

To work on {{ cookiecutter.dist_name }}:

#. Install Pipenv_.
#. Install Python dependencies: ``pipenv install -d``

   This step will also create a virtualenv; you might need to install Python {{ cookiecutter.python_version }}, or tell Pipenv where your existing installation is using the ``--python`` argument. Alternatively, if you have Pyenv_ installed as well, Pipenv will use that.

#. Install pre-commit checks with ``pre-commit install``. (See the section
   below for more details.)
#. Install client-side dependencies and build frontend resources: ``npm install; npm run build``
#. Copy the example settings file:
   ``cp example-settings.ini settings.ini``
#. Edit ``settings.ini``, updating values as appropriate.
#. *(Optional extra step)* Install `devd <https://github.com/cortesi/devd>`_ and `modd <https://github.com/cortesi/modd>`_.

If you followed the optional last step, just run ``modd``; sl-admin will be available at http://localhost:8000/, and the documentation at http://localhost:8000/.docs/. If you didn't:

- To build client-side assets, ``npm run build``, or ``npm run watch`` to run Webpack continuously in watch mode.
- To run Django, ``pipenv run python manage.py runserver``.
- To build documentation, ``cd docs; pipenv run make html``, then open ``docs/_build/html/index.html``.

.. _pipenv: https://docs.pipenv.org/
.. _pyenv: https://github.com/pyenv/pyenv

Project Layout
~~~~~~~~~~~~~~

.. code-block:: none

    docs/               Sphinx documentation sources.
    frontend/           CSS (via Sass) and JavaScript, built using Webpack.
    {{ "%-20s" | format(cookiecutter.dist_name + '/',) }}Root of the Django project.
    apps/               All Django apps go inside here.
    lib/                Global utility functions go in here. This is a Django app
                        too, so you can define e.g. global templatetags in here.
    static/             Global static files go in here. Most static files should go
                        inside their app's static/ subdirectory though.
    templates/          Global templates go in here. Most templates should go inside
                        their app's templates/ subdirectory though.
        base.html       Base template for (nearly) all views.
        base_form.html  Base template for form views.
        partials/       Templates to use in {% raw %}{% include %}{% endraw %} blocks across the project.
    settings.py         Django project settings module.
    Dockerfile          Dockerfile for production builds.
    settings.ini        Your per-environment settings. This file is ignored by Git.

Before You Commit Changes
~~~~~~~~~~~~~~~~~~~~~~~~~

This repository uses `pre-commit <http://pre-commit.com>`__ to enforce
code style (currently in Python only) and check for common errors (such
as hard tabs, CRLF newlines, or changes to your database that don't have
migrations) before you commit. Run ``pre-commit install``
inside your local Git repository to configure it to run before every
commit.

Once it's installed, it will check your code for common mistakes and style
checks before you commit. If your code has mistakes or style problems in it,
it will abort the commit and tell you which checks failed. Some checks will fix
problems for you, in which case you can just run `git diff` to review the
changes, `git add` the changed files, then commit again; other checks will just
warn you about problems which you'll need to fix yourself.

*(pro tip: autopep8 is usually pretty good but it will sometimes wrap
long lines in questionable ways. Pre-commit will give you a chance to
review its changes if it makes any, so make sure you do so.)*

{% if cookiecutter.heroku == "y" %}Deploying on Heroku
Deploying on Heroku
-------------------

.. code:: shell

    heroku create {{ cookiecutter.dist_name }}
    heroku addons:add heroku-postgresql
    heroku addons:add heroku-redis
    heroku addons:add mailgun
    heroku config:set SECRET_KEY=`apg -a1 -m50 -MNCL -n1` \
        ALLOWED_HOSTS="*" \
        EMAIL_FROM_ADDRESS={{ cookiecutter.dist_name }}@example.com{% if not cookiecutter.api_only == "y" %} \
        MEDIA_ROOT=""{% endif %}
        SECURE_PROXY_SSL_HEADER="HTTP_X_FORWARDED_PROTO:https"
    git push heroku master
    heroku run {{ cookiecutter.dist_name }} migrate
    heroku run {{ cookiecutter.dist_name }} createsuperuser

{% endif %}

Building with Docker
--------------------

To build a Dockerfile, just run ``docker build -t {{ cookiecutter.dist_name }} .``.
