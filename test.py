from contextlib import contextmanager
import shutil
import tempfile
from subprocess import check_call
from os.path import join

from cookiecutter.main import cookiecutter

@contextmanager
def tempdir():
    path = tempfile.mkdtemp()
    yield path
    shutil.rmtree(path)


def test_defaults():
    with tempdir() as d:
        cookiecutter('.', output_dir=d, no_input=True,
                     extra_context={'initial_commit': 'n'})
        outdir = join(d, 'myproject')
        check_call(('git', 'init'), cwd=outdir)
        check_call(('pre-commit', 'run', '--all-files'), cwd=outdir)


def test_djangocms():
    with tempdir() as d:
        cookiecutter('.', output_dir=d, no_input=True,
                     extra_context={'django_cms': 'y',
                                    'initial_commit': 'n'})
        outdir = join(d, 'myproject')
        check_call(('git', 'init'), cwd=outdir)
        check_call(('pre-commit', 'run', '--all-files'), cwd=outdir)


def test_webpack():
    with tempdir() as d:
        cookiecutter('.', output_dir=d, no_input=True,
                     extra_context={'webpack': 'y',
                                    'initial_commit': 'n'})
        outdir = join(d, 'myproject')
        check_call(('git', 'init'), cwd=outdir)
        check_call(('pre-commit', 'run', '--all-files'), cwd=outdir)


def test_ldap():
    with tempdir() as d:
        cookiecutter('.', output_dir=d, no_input=True,
                     extra_context={'ldap': 'y',
                                    'initial_commit': 'n'})
        outdir = join(d, 'myproject')
        check_call(('git', 'init'), cwd=outdir)
        check_call(('pre-commit', 'run', '--all-files'), cwd=outdir)
